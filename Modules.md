# Modules
## Examples
- keysanity
- keasy
- homewarp
- glitchless
- intro skip
- open fusion
- progressive items
- entrance rando

## What can modules do
- change item pool
- change logic
- add patches
- add/change shuffle instructions
- add descriptors
- change game data

## module files
[module.yaml spec](module.yaml)

